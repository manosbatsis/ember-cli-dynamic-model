import Ember from 'ember';
import fetch from 'ember-network/fetch';

export default Ember.Service.extend({
  store: Ember.inject.service(),
  loaded_js: new Ember.Object(),
  loading: new Ember.A(),
  loaded_meta: {},
  app_data: undefined,

  fetch_app_meta() {
    const appData = this.get('app_data');
    if (appData !== undefined) {
      return Ember.RSVP.resolve(appData);
    }

    const appConfig = Ember.getOwner(this).resolveRegistration('config:environment');
    const promise = new Ember.RSVP.Promise((resolve, reject) => {
      fetch(`${appConfig.APP.API_HOST}/${appConfig.APP.API_NAMESPACE}/?format=json`, {
        method: 'OPTIONS',
      }).then((response) => {
        response.json().then((json) => {
          this.set('app_data', json);
          resolve(new Ember.Object(json));
        }).catch((err) => {
          reject(err);
        });
      });
    });
    return promise;
  },

  get_model_meta(app, model) {
    const loaded_meta = this.get('loaded_meta');
    if (loaded_meta.hasOwnProperty(`${app}/${model}`)) {
      const data = loaded_meta[`${app}/${model}`];
      if (data.isFulfilled === true || data.isFulfilled === false) {
        return new Ember.RSVP.Promise((resolve, reject) => {
          data.then((json) => {
            resolve(json);
          }).catch((err) => {
            reject(err);
          });
        });
      } else {
        return new Ember.RSVP.Promise((resolve) => {
          resolve(loaded_meta[`${app}/${model}`]);
        });
      }
    } else {
      const appConfig = Ember.getOwner(this).resolveRegistration('config:environment');
      const promise = new Ember.RSVP.Promise((resolve, reject) => {
        fetch(`${appConfig.APP.API_HOST}/${appConfig.APP.API_NAMESPACE}/${app}/${model}/?format=json`, {
          method: 'OPTIONS',
        }).then((response) => {
          response.json().then((json) => {
            const data_obj = Ember.Object.create(json);
            const fieldsets = [];
            data_obj.fieldsets.forEach((fieldset) => {
              fieldsets.push(this.merge_fields_fieldset(data_obj.fields, fieldset));
            });
            data_obj.fieldsets = fieldsets;
            this.get('loaded_meta')[`${app}/${model}`] = new Ember.Object(data_obj);
            resolve(data_obj);
          }).catch((err) => {
            reject(err);
          });
        }).catch((error) => {
          reject(error);
        });
      });
      this.get('loaded_meta')[`${app}/${model}`] = promise;
      return promise;
    }
  },

  merge_fields_fieldset(fields, fieldset) {
    const rv = Ember.Object.create({
      ...fieldset,
      fields: Ember.A()
    });

    if (fieldset.fields === undefined) {
      return rv;
    }

    fieldset.fields.forEach((field) => {
      const meta_field = fields.findBy('name', field.name);
      if (meta_field !== undefined) {
        ['name', 'widget', 'label', 'readonly', 'required', 'translated'].forEach((item) => {
          if (field[item] === undefined && meta_field[item] !== undefined) {
            field[item] = meta_field[item];
          }
        });
        if (field.extra === undefined) {
          field.extra = {};
        }
        Ember.set(field, 'extra', Ember.Object.create(field.extra));

        if (meta_field.extra !== undefined) {
          for (let k in meta_field.extra) {
            if (meta_field.extra.hasOwnProperty(k) && field.extra.get(k) === undefined) {
              field.extra.set(k, meta_field.extra[k]);
            }
          }
        }
      }
      if (field.widget === 'fieldset') {
        field.fields = this.merge_fields_fieldset(fields, field).fields;
      }
      if (field.widget === 'tabset') {
        field.tabs.forEach((tab) => {
          tab.fields = this.merge_fields_fieldset(fields, tab).fields;
        });
      }
      rv.fields.push(Ember.Object.create(field));
    });
    return rv;
  },

  get_model_js(app, singular, plural, resolve, reject) {
    const appConfig = Ember.getOwner(this).resolveRegistration('config:environment');
    const modulePrefix = appConfig.modulePrefix;
    const App = Ember.getOwner(this);
    const file_name = `${app}/${plural}`;
    let loading;
    if (App.hasRegistration(`model:${app}/${singular}`) === false){
      Ember.$.getScript(`/models/${file_name}.js`).done(() => {
        const model_module = window.require(`${modulePrefix}/models/${app}/${singular}`)["default"];
        let registered = false;
        while (!registered) {
          try {
            App.register(`model:${app}/${singular}`, model_module, { singleton: false });
            registered = true;
          } catch (err) {
            if (err.match(/re-register/)) {
              registered = true;
            } else {
              /* eslint-disable no-console */
              console.error(err);
              /* was too fast */
              /* eslint-enable no-console */
            }
          }
        }
        loading = this.get('loading');
        if (loading.indexOf(`${app}/${singular}`) !== -1) {
          this.set('loading', loading.filter((item) => {
            item !== `${app}/${singular};`
          }));
        }
        resolve();
      }).fail((err) => {
        reject(err);
      });
    } else {
      loading = this.get('loading');
      if (loading.indexOf(`${app}/${singular}`) !== -1) {
        this.set('loading', loading.filter((item) => {
          item !== `${app}/${singular};`
        }));
      }
      resolve();
    }
  },

  ensure_model(app, singular, plural, fetchMeta) {

    const loaded_js = this.get('loaded_js');
    if (fetchMeta === undefined) {
      fetchMeta = true;
    }
    if (loaded_js.hasOwnProperty(`${app}/${singular}`)) {
      return new Ember.RSVP.Promise((resolve) => {
        const rv = {app, name: singular};
        if (fetchMeta) {
          rv['meta'] = loaded_js[`${app}/${singular}`]
        }
        resolve(rv);
      });
    } else {
      let promises = [];
      if (plural === undefined) {
        const inflector = new Ember.Inflector(Ember.Inflector.defaultRules);

        plural = inflector.pluralize(singular);
      }

      return new Ember.RSVP.Promise((top_resolve, top_reject) => {
        if (fetchMeta) {
          this.get_model_meta(app, plural).then((json) => {
            promises.push(
              new Ember.RSVP.Promise((resolve, reject) => {
                this.get_model_js(app, singular, plural, resolve, reject);
              }).then(() => {
                this.get('loaded_js')[`${app}/${singular}`] = json;
              })
            );
            if (json.needs) {
              json.needs.forEach((child_model) => {
                const loading = this.get('loading');
                if (loading.indexOf(child_model) === -1)  {
                  loading.pushObject(child_model);
                  promises.push(
                    this.ensure_model(child_model.app, child_model.singular, child_model.plural, fetchMeta)
                  );
                }
              });
            }
            Ember.RSVP.Promise.all(promises).then(() => {
              top_resolve({app, name: singular, meta: json});
            }).catch((error) => {
              top_reject(error);
            });
          }).catch((err) => {
            top_reject(err);
          });
        } else {
          this.get_model_js(app, singular, plural, top_resolve, top_reject);
        }
      });
    }
  },
});
